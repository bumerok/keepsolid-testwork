<?php

namespace KeepSolid\TestTask\Exceptions;

use Throwable;

class ValidateFileException extends \Exception
{
    const INVALID_FILE_TYPE_MESSAGE = '';
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
