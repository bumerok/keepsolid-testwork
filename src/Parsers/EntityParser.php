<?php

namespace KeepSolid\TestTask\Parsers;

use InvalidArgumentException;
use ReflectionClass;
use ReflectionProperty;

/**
 * Class EntityParser
 *
 * @package KeepSolid\TestTask\Parsers
 */
class EntityParser
{
    /**
     * @var \stdClass
     */
    protected $entityClass;

    /**
     * @var array
     */
    protected $parent;

    /**
     * @var string Key in JSON for identification entity
     */
    protected $entityMapKey;

    /**
     * @var array
     */
    protected static $mappedProperties = [];

    const PROPERTY_TO_PARSER_MAP = [
        'hotels' => HotelParser::class,
        'partners' => PartnerParser::class,
        'prices' => PriceParser::class,
    ];

    /**
     * @param array $data
     *
     * @throws InvalidArgumentException
     *
     * @return mixed
     */
    public function buildEntity(array $data)
    {
        $resultEntities = [];

        foreach ($data as $entityData) {
            $entity = new $this->entityClass;
            $this->setEntityProperties($entity, $entityData);
            $resultEntities[] = $entity;
        }

        return $resultEntities;
    }

    /**
     * @param ReflectionProperty $property
     * @param     array          $entityArray
     *
     * @return string $propertyName
     */
    public function validateProperty(ReflectionProperty $property, $entityArray)
    {
        $propertyName = $property->getName();
        if (isset($entityArray[$propertyName])) {
            return $propertyName;
        }
        // Try to find property in mapped array
        $propertyName = $this->getMappedPropertyName($propertyName);

        if (isset($entityArray[$propertyName])) {
            return $propertyName;
        }
        throw new InvalidArgumentException("Invalid value for property '$propertyName'");
    }

    /**
     * Prepare property value id needs
     *
     * @param   mixed $value
     * @param string  $propertyName
     *
     * @return mixed
     */
    public function prepareProperty(string $propertyName, $value)
    {
        return $value;
    }

    /**
     * @return ReflectionProperty[]
     */
    private function getEntityProperties()
    {
        $reflection = new ReflectionClass($this->entityClass);

        return $reflection->getProperties(ReflectionProperty::IS_PUBLIC);
    }

    /**
     * @param       $entity
     * @param array $entityData
     *
     * @return mixed
     */
    private function setEntityProperties($entity, array $entityData)
    {
        foreach ($this->getEntityProperties() as $property) {
            $propertyName = $property->getName();
            $mappedKey = $this->validateProperty($property, $entityData);

            // Check and build child entities
            if (isset(self::PROPERTY_TO_PARSER_MAP[$propertyName])) {
                $childEntityParser = $this->getParserInstanceByPropertyName($propertyName, $entityData);
                $entity->$propertyName = $childEntityParser->parse();
                continue;
            }

            $entity->$propertyName = $this->prepareProperty($mappedKey, $entityData[$mappedKey]);
        }

        return $entity;
    }

    /**
     * Create instance of parser using property name and parent data
     *
     * @param string $propertyName
     * @param array  $parentData
     *
     * @return ParserInterface
     */
    private function getParserInstanceByPropertyName(string $propertyName, array $parentData)
    {
        $className = self::PROPERTY_TO_PARSER_MAP[$propertyName];

        return new $className($parentData);
    }

    /**
     * Get mapped key for current property name
     *
     * @param string $propertyName
     *
     * @return string
     * @throws InvalidArgumentException
     */
    private function getMappedPropertyName(string $propertyName): string
    {
        if (!isset(static::$mappedProperties[$propertyName])) {
            throw new InvalidArgumentException("Invalid value for property '$propertyName'");
        }

        return static::$mappedProperties[$propertyName];
    }
}
