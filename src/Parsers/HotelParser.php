<?php

namespace KeepSolid\TestTask\Parsers;

use KeepSolid\TestTask\Entity\Hotel;

/**
 * Class HotelParser
 *
 * @package KeepSolid\TestTask\Parsers
 */
class HotelParser extends EntityParser implements ParserInterface
{
    /**
     * @var array
     */
    protected static $mappedProperties = [
        'address' => 'adr',
    ];

    /**
     * @var string
     */
    protected $entityClass = Hotel::class;

    /**
     * HotelParser constructor.
     *
     * @param array $city
     */
    public function __construct(array $city)
    {
        if (empty($city) || !is_array($city)) {
            throw new \InvalidArgumentException('City data can not be blank and must be an array');
        }
        $this->parent = $city;
        $this->entityMapKey = 'hotels';

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function parse()
    {
        if (!isset($this->parent[$this->entityMapKey])) {
            throw new  \ErrorException('This city has no hotels');
        }

        return $this->buildEntity($this->parent[$this->entityMapKey]);
    }
}
