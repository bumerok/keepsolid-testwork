<?php

namespace KeepSolid\TestTask\Parsers;

interface ParserInterface
{
    /**
     * Set data from parent array into entity structure
     *
     * @return mixed Instance of current entity
     * @throws \ErrorException if current entity map key not found in relative array
     */
    public function parse();
}
