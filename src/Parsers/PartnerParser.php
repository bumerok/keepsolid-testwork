<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 06.07.17
 * Time: 2:45
 */

namespace KeepSolid\TestTask\Parsers;

use KeepSolid\TestTask\Entity\Partner;

/**
 * Class PartnerParser
 *
 * @package KeepSolid\TestTask\Parsers
 */
class PartnerParser extends EntityParser implements ParserInterface
{
    /**
     * @var array
     */
    protected static $mappedProperties = [
        'arrivalDate' => 'from',
        'departureDate' => 'to',
        'homepage' => 'url'
    ];

    /**
     * @var string
     */
    protected $entityClass = Partner::class;

    /**
     * PartnerParser constructor.
     *
     * @param array $hotel
     */
    public function __construct(array $hotel)
    {
        if (empty($hotel) || !is_array($hotel)) {
            throw new \InvalidArgumentException('Hotel data can not be blank and must be an array');
        }
        $this->entityMapKey = 'partners';
        $this->parent = $hotel;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function parse()
    {
        if (!isset($this->parent[$this->entityMapKey])) {
            throw new \ErrorException('This Hotel has no partners');
        }

        return $this->buildEntity($this->parent[$this->entityMapKey]);
    }
}
