<?php

namespace KeepSolid\TestTask\Parsers;

use KeepSolid\TestTask\Entity\Price;

/**
 * Class PriceParser
 *
 * @package KeepSolid\TestTask\Parsers
 */
class PriceParser extends EntityParser implements ParserInterface
{
    /**
     * @var array
     */
    protected static $mappedProperties = [
        'arrivalDate' => 'from',
        'departureDate' => 'to',
    ];

    /**
     * @var string
     */
    protected $entityClass = Price::class;

    /**
     * PriceParser constructor.
     *
     * @param array $partner
     */
    public function __construct(array $partner)
    {
        if (empty($partner) || !is_array($partner)) {
            throw new \InvalidArgumentException('Partner data can not be blank and must be an array');
        }
        $this->entityMapKey = 'prices';
        $this->parent = $partner;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function parse()
    {
        if (!isset($this->parent[$this->entityMapKey])) {
            throw new \ErrorException('This Partner has no prices');
        }

        return $this->buildEntity($this->parent[$this->entityMapKey]);
    }

    /**
     * @inheritdoc
     */
    public function prepareProperty(string $propertyName, $value)
    {
        switch ($propertyName) {
            case 'from':
            case 'to':
                return new \DateTime(trim($value));
            case 'amount':
                return is_scalar($value) ? floatval($value) : 0.0;
        }

        return $value;
    }

}
