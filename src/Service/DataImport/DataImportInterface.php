<?php

namespace KeepSolid\TestTask\Service\DataImport;

/**
 * Interface DataImportInterface
 *
 * @package KeepSolid\TestTask\Service\DataImport
 */
interface DataImportInterface
{
    /**
     * Get files names from specific directory
     *
     * @return string[] array Files need For import
     */
    public function getFileNamesForImport(): array;

    /**
     * Validate file content
     *
     * @param string $filePath
     *
     * @return boolean
     */
    public function validateFileContent(string $filePath);

    /**
     * Parse data from file and return it as array
     *
     * @param string $filePath
     *
     * @return array
     */
    public function parseFile(string $filePath): array;
}
