<?php

namespace KeepSolid\TestTask\Service\DataImport;

/**
 * Class DataImportService
 *
 * @package KeepSolid\TestTask\Service\DataImport
 */
class DataImportService implements DataImportInterface
{
    /**
     * Path of directory with data files
     */
    const DATA_DIR_PATH = 'data/';

    /**
     * @inheritdoc
     */
    public function getFileNamesForImport(): array
    {
        $fileNames = glob(self::DATA_DIR_PATH . '*.json');

        if (empty($fileNames)) {
            throw new \ErrorException('Has no files to import');
        }

        return $fileNames;
    }

    /**
     * @inheritdoc
     */
    public function validateFileContent(string $filePath)
    {
        return file_exists($filePath) && is_array(json_decode(file_get_contents($filePath), true));
    }

    /**
     * @inheritdoc
     */
    public function parseFile(string $filePath): array
    {
        return json_decode(file_get_contents($filePath), true);
    }
}
