<?php

namespace KeepSolid\TestTask\Service;

use KeepSolid\TestTask\Parsers\HotelParser;
use KeepSolid\TestTask\Service\DataImport\DataImportService;

/**
 * Class UnorderedPartnerService
 *
 * @package KeepSolid\TestTask\Service
 */
class UnorderedPartnerService implements PartnerServiceInterface
{
    /**
     * @var DataImportService
     */
    private $dataImportService;

    /**
     * @var array
     */
    public $parsedData;

    /**
     * UnorderedPartnerService constructor.
     */
    public function __construct()
    {
        $this->dataImportService = new DataImportService();
    }

    /**
     * @param int $cityId
     *
     * @return array
     * @throws \Exception
     */
    public function getResultForCityId(int $cityId): array
    {
        $cities = $this->getCities();
        $mappedCities = $this->mapCitiesIdToHotels($cities);
        if (!isset($mappedCities[$cityId])) {
            throw new \Exception("City with id $cityId is not found");
        }
        if (empty($mappedCities[$cityId])) {
            throw new \Exception("City with id $cityId has no hotels");
        }

        $hotelsParser = new HotelParser($mappedCities[$cityId]);

        return $hotelsParser->parse();
    }

    /**
     * Get cities parsed from files
     *
     * @return array
     */
    private function getCities()
    {
        $cities = [];
        $fileNames = $this->dataImportService->getFileNamesForImport();

        foreach ($fileNames as $fileName) {
            if ($this->dataImportService->validateFileContent($fileName)) {
                $cities = $this->dataImportService->parseFile($fileName);
            }
        }

        return $cities;
    }

    /**
     * @param array $city
     *
     * @return array
     * @throws \Exception
     */
    private function mapCitiesIdToHotels(array $city)
    {
        $mappedCities = [];
        if (!isset($city['id'])) {
            throw new \Exception("City id is required");
        }
        if (!isset($city['hotels'])) {
            throw new \Exception("Has no hotels in city");
        }
        $mappedCities[$city['id']] = $city;

        return $mappedCities;
    }
}
